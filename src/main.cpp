#include "../include/ResourceManager.hpp"
#include "../include/SpriteSheet.hpp"

int main()
{
	at::TextureManager textureManager;

	//loading frames:
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 0, 0, 250, 250 }, "gear_frame0", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 250, 0, 250, 250 }, "gear_frame1", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 500, 0, 250, 250 }, "gear_frame2", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 750, 0, 250, 250 }, "gear_frame3", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 0, 250, 250, 250 }, "gear_frame4", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 250, 250, 250, 250 }, "gear_frame5", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 500, 250, 250, 250 }, "gear_frame6", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 750, 250, 250, 250 }, "gear_frame7", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 0, 500, 250, 250 }, "gear_frame8", textureManager);
	at::loadTextureFromSpriteSheet("resources/animated_gear_spritesheet.png", sf::IntRect{ 250, 500, 250, 250 }, "gear_frame9", textureManager);
	
	sf::RenderWindow window{ sf::VideoMode{ 250, 250 }, "AnimationTest" };
	sf::Sprite sprite{ *textureManager.getResource("gear_frame0") };
	while (window.isOpen())
	{
		sf::Event event;
		window.pollEvent(event);
		switch (event.type)
		{
		case sf::Event::Closed:
			window.close();
		default:
			break;
		}
		window.clear(sf::Color::White);
		window.draw(sprite);
		window.display();
	}
}